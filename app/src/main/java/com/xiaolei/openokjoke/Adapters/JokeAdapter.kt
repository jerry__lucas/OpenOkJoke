package com.xiaolei.openokjoke.Adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.umeng.socialize.media.UMImage
import com.xiaolei.okhttputil.Catch.Interfaces.CacheInterface
import com.xiaolei.openokjoke.Base.BaseHolder
import com.xiaolei.openokjoke.Base.RecyclerAdapter
import com.xiaolei.openokjoke.Beans.JokeBean
import com.xiaolei.openokjoke.Cache.CacheUtil
import com.xiaolei.openokjoke.Configs.Globals
import com.xiaolei.openokjoke.Net.APPNet
import com.xiaolei.openokjoke.Net.BaseRetrofit
import com.xiaolei.openokjoke.R
import com.xiaolei.openokjoke.UM.UMShare
import java.util.*
import com.xiaolei.openokjoke.Exts.*


/**
 * Created by xiaolei on 2018/3/8.
 */
class JokeAdapter(mlist: LinkedList<JokeBean>,
                  val context: Activity?) : RecyclerAdapter<JokeBean>(mlist)
{
    lateinit var cacheImpl: CacheInterface
    private val requestOptions = RequestOptions().centerCrop()
    private val umShare = UMShare(context)
    private val appNet by lazy {
        BaseRetrofit.create(APPNet::class.java)
    }

    override fun onCreate(parent: ViewGroup, viewType: Int): BaseHolder
    {
        if (!::cacheImpl.isInitialized)
        {
            cacheImpl = CacheUtil.getCacheInstance(parent.context)
        }

        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_joke_item, parent, false)
        val holder = BaseHolder(view)
        val zhezhao = holder.get<View>(R.id.zhezhao)
        val context_img = holder.get<ImageView>(R.id.context_img)

        val jubao = holder.get<ImageView>(R.id.jubao)
        val share = holder.get<ImageView>(R.id.share)
        share.setTag(R.id.recycle_item_rootview, holder.rootView)

        jubao.setOnClickListener {
            val bean = it.getTag(R.id.recycle_item_bean) as JokeBean
            doReport(bean, it)
        }
        share.setOnClickListener {
            val bean = it.getTag(R.id.recycle_item_bean) as JokeBean
            val rootView = it.getTag(R.id.recycle_item_rootview) as ViewGroup
            doShare(bean, rootView)
        }

        zhezhao.setOnClickListener {
            val tag = zhezhao.tag as? String?
            tag?.let {
                Glide.with(context_img)
                        .load(tag)
                        .apply(requestOptions)
                        .into(context_img)
                cacheImpl.put(tag, "1") // 点击之后，添加一条记录
                zhezhao.gone()
            }
        }
        return holder
    }

    override fun onBindView(holder: BaseHolder, position: Int)
    {
        val title = holder.get<TextView>(R.id.title)
        val context_text = holder.get<TextView>(R.id.context_text)
        val context_img = holder.get<ImageView>(R.id.context_img)
        val img_context = holder.get<View>(R.id.img_context)
        val zhezhao = holder.get<View>(R.id.zhezhao)
        val jubao = holder.get<ImageView>(R.id.jubao)
        val share = holder.get<ImageView>(R.id.share)
        val bean = list[position]
        val context = holder.rootView.context
        val sp = context.getSharedPreferences(Globals.spfile, Context.MODE_PRIVATE)

        jubao.setTag(R.id.recycle_item_bean, bean) // 举报
        share.setTag(R.id.recycle_item_bean, bean) // 分享


        title.text = bean.title
        when (bean.type)
        {
            JokeBean.Type.txt.name ->
            {
                img_context.gone()
                context_text.show()
                context_text.text = bean.content
            }
            JokeBean.Type.jpg.name, JokeBean.Type.gif.name ->
            {
                img_context.show()
                context_text.gone()
                val autoLoadGif = sp.getBoolean("autoLoadGif", true)
                if (autoLoadGif)
                {
                    zhezhao.gone()
                    Glide.with(holder.rootView)
                            .load(bean.content)
                            .apply(requestOptions)
                            .into(context_img)
                    if (!cacheImpl.containsKey(bean.content)) // 是否已经加载过
                    {
                        // 如果没有加载过
                        cacheImpl.put(bean.content, "1") // 添加一条已经加载过的记录
                    }
                } else
                {
                    if (cacheImpl.containsKey(bean.content)) // 是否已经加载过
                    {  // 已经加载过
                        zhezhao.gone() // 隐藏遮罩
                        Glide.with(holder.rootView)
                                .load(bean.content)
                                .apply(requestOptions)
                                .into(context_img) // 直接加载图片
                    } else
                    {
                        // 没有加载过、
                        // 显示遮罩
                        zhezhao.show()
                        zhezhao.tag = bean.content
                    }
                }
            }
        }
    }

    /**
     * do 举报
     */
    private fun doReport(bean: JokeBean, view: View)
    {
        val context = view.context
        view.isClickable = false
        val call = appNet.report("${bean.id}", context?.getImei())
        call.enqueue(context, { result ->
            Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
        }, {
            view.isClickable = true
        })
    }


    /**
     * do 分享
     */
    private fun doShare(bean: JokeBean, viewParent: ViewGroup)
    {
        when (bean.type)
        {
            JokeBean.Type.txt.name ->
            {
                val bitmap = viewParent.buildBitmap()
                umShare.shareImage(UMImage(context, bitmap)).open()
            }
            JokeBean.Type.jpg.name, JokeBean.Type.gif.name ->
            {
                umShare.shareImage(UMImage(context, bean.content)).open()
            }
        }
    }
}