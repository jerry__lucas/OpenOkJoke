package com.xiaolei.openokjoke.Configs

/**
 * 这里存放所有eventBus的指令
 * Created by xiaolei on 2018/3/13.
 */
object EventAction
{
    const val closeDrawer = 1   // 关闭打开抽屉
    const val refreshSuiji = 2  // 刷新推荐界面
    const val refreshTxt = 3    // 刷新文本界面 
    const val refreshJpg = 4    // 刷新图片界面
    const val refreshGif = 5    // 刷新动图界面
    
    const val refreshIng = 6    // 正在刷新中
    const val refreshEnd = 7    // 刷新完成
}