package com.xiaolei.openokjoke.Base

import android.graphics.Color
import android.os.Build
import android.view.View
import com.githang.statusbar.StatusBarCompat
import com.xiaolei.easyfreamwork.base.BaseActivity
import com.xiaolei.openokjoke.BuildConfig
import com.umeng.analytics.MobclickAgent



/**
 * Created by xiaolei on 2017/12/6.
 */

abstract class BaseActivity : BaseActivity()
{
    override fun onSetContentView()
    {
        setStatusBar(false)
    }
    
    fun setStatusBar(lightStatusBar: Boolean)
    {
        if (Build.VERSION.SDK_INT >= 21)
        {
            val decorView = window.decorView
            val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            decorView.systemUiVisibility = option
            window.statusBarColor = Color.TRANSPARENT
            StatusBarCompat.setStatusBarColor(this, Color.TRANSPARENT, lightStatusBar)
        }
    }
    
    /**
     * 只有在DEBUG模式下，才会弹出
     *
     * @param object
     */
    fun AlertDebug(`object`: Any)
    {
        if (BuildConfig.DEBUG)
        {
            Alert(`object`)
        }
    }

    override abstract fun initObj()
    override abstract fun initView()
    override abstract fun initData()
    override abstract fun setListener()
    override abstract fun loadData()

    public override fun onResume()
    {
        super.onResume()
        MobclickAgent.onResume(this)
    }

    public override fun onPause()
    {
        super.onPause()
        MobclickAgent.onPause(this)
    }
}